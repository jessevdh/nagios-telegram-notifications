# Nagios Telegram Notifications

Python script using TWX.botapi to send nagios notifications to telegram. This can either be a person or a group.

## Create Telegram bot

Talk to @BotFather
```
/newbot
my_bot_name
my_bot_username
```

You will receive an API token which is needed to send notifications.

EXAMPLE:
Token: 123456789:sdfjihh84hg8r49fj49j34odjkr94j9fjr9
UserID: 123456789

If you want the bot to talk in a group get the GroupID.
Create a group and add the bot to it.
Send a few messages to the group.

REPLACE WITH YOUR OWN API KEY
https://api.telegram.org/bot557776685:AAGe4Mt7vZXQL0vXzrvpNONRI-XTTgf3zxY/getUpdates

Get the chat ID (it's a negative value).
chat":{"id":-12345678

## Prepare Nagios server

download the nagios_telegram.py file to your machine.
Place the file in /usr/local/bin/
chmod 755 the file

Install twx.botapi

```
pip install twx.botapi
```

You will need this config in the commands config file. Replace the token with your own!

```
define command {
  command_name     notify-host-by-telegram
  command_line     /usr/local/bin/telegram_nagios.py --token 123456789:sdfjihh84hg8r49fj49j34odjkr94j9fjr9 --object_type host --contact "$CONTACTPAGER$" --notificationtype "$NOTIFICATIONTYPE$" --hoststate "$HOSTSTATE$" --hostname "$HOSTNAME$" --hostaddress "$HOSTADDRESS$" --output "$HOSTOUTPUT$"
}
define command {
  command_name     notify-service-by-telegram
  command_line     /usr/local/bin/telegram_nagios.py --token 123456789:sdfjihh84hg8r49fj49j34odjkr94j9fjr9 --object_type service --contact "$CONTACTPAGER$" --notificationtype "$NOTIFICATIONTYPE$" --servicestate "$SERVICESTATE$" --hostname "$HOSTNAME$" --servicedesc "$SERVICEDESC$" --output "$SERVICEOUTPUT$"
}
```

You will need this in the contacts config file. Replace with your own group chat ID.

```
define contact {
  contact_name                    telegram
  pager                           -12345678
  service_notification_commands   notify-service-by-telegram
  host_notification_commands      notify-host-by-telegram
}
```

## after installation

You can test your bot using the following commands. Replace with your own API key.

```
telegram_nagios.py --token 123456789:sdfjihh84hg8r49fj49j34odjkr94j9fjr9 --object_type service --contact "-23456789" --servicestate "OK" --hostname "hostname.domain.tld" --servicedesc "load" --output "OK - load average: 0.02 0.01 0.01"
telegram_nagios.py --token 123456789:sdfjihh84hg8r49fj49j34odjkr94j9fjr9 --object_type host --contact "-23456789" --hoststate "DOWN" --hostname "hostname.domain.tld" --hostaddress "2001:DB8::1" --output "PING CRITICAL - Packet loss = 100%"
```

If it works you're basically done. Just add the "telegram" contact to the hosts or services you want to receive telegram notifications for.